/*
 This class is a Data Access Object alow  all automatic operations, wich want to access to database
it implement the singelton model to have only one access instance and avoid problem DB Access
 */
package esprit.zendah.DAO;

import esprit.zendah.db.Connexion;
import esprit.zendah.entities.Levy;
import esprit.zendah.entities.Moisture;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Khalil
 */
public class AutomaticOperationsDAO {

    private static Connection cnx;
    private static AutomaticOperationsDAO usrDAO;

    private AutomaticOperationsDAO() {
        cnx = Connexion.getInstance().getConn();
    }

    public static AutomaticOperationsDAO getInstance() {
        if (usrDAO == null) {
            usrDAO = new AutomaticOperationsDAO();
        }
        return usrDAO;
    }

    public Moisture getDefaultMoistureValues() throws SQLException {

        java.sql.PreparedStatement smt;
        smt = cnx.prepareStatement("select * from default_moisture ");
        ResultSet rs = smt.executeQuery();
        rs.next();
        //System.out.println("rs" + rs.toString());
        System.out.println("rs" + rs.getInt("id"));
        Moisture us = new Moisture(rs.getInt("id"), rs.getString("max_wet"), rs.getString("min_wet"), rs.getString("right_wet"));
        smt.close();
        return us;

    }

   

    public void deleteLevy(int id) throws SQLException {
//		java.sql.PreparedStatement smt;
//
//		smt = cnx.prepareStatement("delete from levies where us_id=?");
//		smt.setInt(1, id);
//		smt.executeUpdate();
//		System.out.println("suppression effectuer avec succees");
    }


    public void updateLevy(Levy usr) throws SQLException {
//		java.sql.PreparedStatement smt;
//
//		smt = cnx
//				.prepareStatement("update users set us_ic_id=?,us_nom=?,us_prenom=?,us_mail=?,us_login=?,us_pwd where us_id=?");
//		smt.setInt(1, usr.getId_camera());
//		smt.setString(2, usr.getNom());
//		smt.setString(3, usr.getPrenom());
//		smt.setString(4, usr.getMail());
//		smt.setString(5, usr.getLogin());
//		smt.setString(6, usr.getPassword());
//		smt.setInt(7, usr.getId_user());
//
//		smt.executeUpdate();
//		System.out.println("modification effectuer avec succees");

    }

}
