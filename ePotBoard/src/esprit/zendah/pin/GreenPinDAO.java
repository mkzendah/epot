/*
Green Leds register access, pin D4 , gpio28
 */
package esprit.zendah.pin;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Khalil
 */
public class GreenPinDAO {

    ConfigurationGPIO cp;

    public void initReading() {

        cp = new ConfigurationGPIO();
//        cp.init();
        cp.MyPin="28";
        //cp.getPin(14);
        cp.unexport();
        cp.export();
        cp.setDrive("strong");
        cp.setMode("out");
        cp.setData("0");
        
    }
    
    public void ledOn() {
    cp.setData("1");
    }
    public void ledOff() {
    cp.setData("0");
    }

//    public String readFromFile() {
//        String path = "/sys/class/gpio/gpio28/value";
//        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
//
//            String sCurrentLine;
//
//            while ((sCurrentLine = br.readLine()) != null) {
//                System.out.println(sCurrentLine);
//                return sCurrentLine;
//            }
//
//        } catch (IOException e) {
//            System.out.println("++++exception : " + e);
//        }
//
//        return "";
//    }
//
    public void stopUse() {
        cp.unexport();
    }

}
