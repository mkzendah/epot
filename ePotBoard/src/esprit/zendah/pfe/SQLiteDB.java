//
//package esprit.zendah.pfe;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author Khalil
// */
//public class SQLiteDB {
//
//    private static SQLiteDB INSTANCE;
//    Connection c = null;
//    Statement stmt = null;
////    SQLiteConnection db;
////    SQLiteStatement st;
//// Private constructor prevents instantiation from other classes
//
//    private SQLiteDB() {
//        try {
//            Class.forName("org.sqlite.JDBC");
//            c = DriverManager.getConnection("jdbc:sqlite:ipot.db");
//
//        } catch (ClassNotFoundException | SQLException ex) {
//            System.out.println("Instantiation SQLiteException: " + ex.getMessage());
//        }
//    }
//
//    public static SQLiteDB getInstance() {
//        if (INSTANCE == null) {
//            INSTANCE = new SQLiteDB();
//        }
//        return INSTANCE;
//    }
//    
//    
//    
//    
//
//    public boolean exec(String str) {
//        try {
//            stmt.executeUpdate(str);
//
//            return true;
//        } catch (SQLException ex) {
//            System.out.println("Query Execution SQLiteException: " + ex.getMessage());
//            return false;
//        }
//    }
//
////    public boolean prepareQuery(String str) {
////        try {
////            st = db.prepare(str);
////        } catch (SQLiteException ex) {
////            System.out.println("Prepare Query SQLiteException: " + ex.getMessage());
////            return false;
////        }
////        return true;
////    }
////    public Object[] fetch() {
////        try {
////            if (!st.step()) {
////                st.dispose();
////                return null;
////            } else {
////                if (st.hasRow()) {
////                    int columns = st.columnCount();
////                    Stack stack = new Stack();
////                    for (int column = 0; column < columns; column++) {
////                        stack.push(st.columnValue(column));
////                    }
////                    return stack.toArray();
////                } else {
////                    st.dispose();
////                    return null;
////                }
////            }
////        } catch (SQLiteException ex) {
////            System.out.println("Fetch SQLiteException: " + ex.getMessage());
////        }
////        st.dispose();
////        return null;
////    }
//    public void close() {
//        try {
//            c.close();
//        } catch (SQLException ex) {
//            Logger.getLogger(SQLiteDB.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//}
