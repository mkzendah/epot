/*
 This class takke care of the automatic levies, it read from sensors and write values in DB
 */
package esprit.zendah.pfe;

import esprit.zendah.DAO.LevyDAO;
import esprit.zendah.entities.Levy;
import esprit.zendah.pin.LightSensorPinDAO;
import esprit.zendah.pin.MoisturePinDAO;
import esprit.zendah.pin.TemperaturePinDAO1;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Khalil
 */
public class LeviesManager extends Thread {

    MoisturePinDAO ms;
    LightSensorPinDAO ls;
    TemperaturePinDAO1 tp;
    int sleepPeriode;

    public LeviesManager(int sleepPeriod) {
        this.sleepPeriode = sleepPeriod;

        ms = new MoisturePinDAO();
        tp = new TemperaturePinDAO1();
        ls = new LightSensorPinDAO();

        ms.initReading();
        ls.initReading();
        tp.initReading();

    }

    @Override
    public void run() {
        while (true) {

            String moisture = ms.readFromFile();
            String light = ls.readFromFile();
            String temperature = tp.readFromFile();

            Calendar cal;
            DateFormat dateFormat
                    = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
            String dateStr;
            cal = Calendar.getInstance();
            dateStr = dateFormat.format(cal.getTime());

            Levy lv = new Levy(dateStr, moisture, light, temperature);
            try {
                LevyDAO.getInstance().addLevy(lv);
            } catch (SQLException ex) {
                Logger.getLogger(LeviesManager.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.print("Light : " + light);
            System.out.print("temperature : " + temperature);
            System.out.print("moisture : " + moisture);

            try {
                Thread.currentThread().sleep(sleepPeriode);
            } catch (InterruptedException ex) {
                Logger.getLogger(LeviesManager.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
