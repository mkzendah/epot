/*
 I used this class to define the automati mode, I compare the soil moisture value to the min moisture value
 of my plant profile. if It's under this one, the water tap will be opened during 1 seconde, wait 15min and control
 the soil moisture another time, until the soil attemp the right moisture value, depending on plant profil
 */
package esprit.zendah.pfe;

import esprit.zendah.DAO.AutomaticOperationsDAO;
import esprit.zendah.entities.Moisture;
import esprit.zendah.pin.MoisturePinDAO;
import esprit.zendah.pin.VannePinDAO1;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Khalil
 */
public class OperationsManager extends Thread {

    Moisture ms;
    VannePinDAO1 vanne;
    MoisturePinDAO moistrePin;

    private static OperationsManager INSTANCE;
    static int controlMoisturePeriode = 900000;//levies every 15 minute
    static int workingMode = 0;

    @Override
    public void run() {
        ms = new Moisture();

        moistrePin = new MoisturePinDAO();
        vanne = new VannePinDAO1();

        moistrePin.initReading();
        vanne.initReading();

        try {
            ms = AutomaticOperationsDAO.getInstance().getDefaultMoistureValues();
            System.out.println("ms : " + ms.toString());
        } catch (SQLException ex) {
            Logger.getLogger(OperationsManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (true) {

            if (Integer.valueOf(moistrePin.readFromFile()) < Integer.valueOf(ms.getMin_wet()) )  {

                LightsManager lm = new LightsManager().getInstance();
                lm.redCube();
                vanne.vanneOn();

                try {
                    Thread.currentThread().sleep(2000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(OperationsManager.class.getName()).log(Level.SEVERE, null, ex);
                }

                vanne.vanneOff();
            }
            LightsManager lm = new LightsManager().getInstance();
            lm.greenCube();
            // System.out.println("I am greenCube from opMAnger");

            try {
                Thread.currentThread().sleep(controlMoisturePeriode);
            } catch (InterruptedException ex) {
                Logger.getLogger(OperationsManager.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
