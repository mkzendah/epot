/*
 this is the Levy class entity, with all its methods and atributes
 */

package esprit.zendah.entities;

/**
 *
 * @author Khalil
 */
public class Levy {
    
    int id;
    String date;
    String moisture;
    String light;
    String temperature;

    public Levy() {
    }

    public Levy(String date, String moisture, String light, String temperature) {
        this.date = date;
        this.moisture = moisture;
        this.light = light;
        this.temperature = temperature;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getMoisture() {
        return moisture;
    }

    public String getLight() {
        return light;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setMoisture(String moisture) {
        this.moisture = moisture;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }
    
    
    
}
