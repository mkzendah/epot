/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.java.net.zendah;

public final class R {
    public static final class attr {
        /** <p>Must be a color value, in the form of "<code>#<i>rgb</i></code>", "<code>#<i>argb</i></code>",
"<code>#<i>rrggbb</i></code>", or "<code>#<i>aarrggbb</i></code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pstsDividerColor=0x7f010002;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pstsDividerPadding=0x7f010005;
        /** <p>Must be a color value, in the form of "<code>#<i>rgb</i></code>", "<code>#<i>argb</i></code>",
"<code>#<i>rrggbb</i></code>", or "<code>#<i>aarrggbb</i></code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pstsIndicatorColor=0x7f010000;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pstsIndicatorHeight=0x7f010003;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pstsScrollOffset=0x7f010007;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pstsShouldExpand=0x7f010009;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int pstsTabBackground=0x7f010008;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pstsTabPaddingLeftRight=0x7f010006;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pstsTextAllCaps=0x7f01000a;
        /** <p>Must be a color value, in the form of "<code>#<i>rgb</i></code>", "<code>#<i>argb</i></code>",
"<code>#<i>rrggbb</i></code>", or "<code>#<i>aarrggbb</i></code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pstsUnderlineColor=0x7f010001;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int pstsUnderlineHeight=0x7f010004;
    }
    public static final class color {
        public static final int background_tab_pressed=0x7f040000;
        public static final int background_window=0x7f040001;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
    }
    public static final class drawable {
        public static final int actionbar_bottom=0x7f020000;
        public static final int background_card=0x7f020001;
        public static final int background_tab=0x7f020002;
        public static final int background_tabs=0x7f020003;
        public static final int background_tabs_diagonal=0x7f020004;
        public static final int bt_spray_pressed=0x7f020005;
        public static final int bt_spray_unpressed=0x7f020006;
        public static final int bt_tools=0x7f020007;
        public static final int bt_tools_refresh=0x7f020008;
        public static final int contact=0x7f020009;
        public static final int goutte_blanc=0x7f02000a;
        public static final int goutte_red=0x7f02000b;
        public static final int ic_action_user=0x7f02000c;
        public static final int ic_launcher=0x7f02000d;
        public static final int ic_launcher_actionbar=0x7f02000e;
        public static final int ic_launcher_chrome=0x7f02000f;
        public static final int ic_launcher_gmail=0x7f020010;
        public static final int ic_launcher_gmaps=0x7f020011;
        public static final int ic_launcher_gplus=0x7f020012;
        public static final int light_icon=0x7f020013;
        public static final int moisture_icon=0x7f020014;
        public static final int refresh_icon=0x7f020015;
        public static final int refresh_icon_pressed=0x7f020016;
        public static final int splash=0x7f020017;
        public static final int statistic=0x7f020018;
        public static final int tabs_pattern=0x7f020019;
        public static final int tabs_pattern_diagonal=0x7f02001a;
        public static final int temperature_icon=0x7f02001b;
    }
    public static final class id {
        public static final int action_contact=0x7f090015;
        public static final int bt_ok_setting=0x7f09000d;
        public static final int bt_refresh=0x7f09000f;
        public static final int bt_spray=0x7f090007;
        public static final int center=0x7f09000e;
        public static final int colors=0x7f090002;
        public static final int frag_contact=0x7f090008;
        public static final int image=0x7f090009;
        public static final int ipadress=0x7f09000b;
        public static final int light_cercle=0x7f090014;
        public static final int pager=0x7f090001;
        public static final int port=0x7f09000c;
        public static final int progressBar1=0x7f090003;
        public static final int tabs=0x7f090000;
        public static final int temperature_txt_data=0x7f090005;
        public static final int tmp_circle=0x7f090011;
        public static final int txt_Light=0x7f090013;
        public static final int txt_custum_title=0x7f090004;
        public static final int txt_moistue=0x7f090010;
        public static final int txt_temperature=0x7f090012;
        public static final int txt_title=0x7f09000a;
        public static final int water_gt=0x7f090006;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int activity_splash=0x7f030001;
        public static final int custum_spray=0x7f030002;
        public static final int fragment_quick_contact=0x7f030003;
        public static final int param=0x7f030004;
        public static final int stats=0x7f030005;
    }
    public static final class menu {
        public static final int main=0x7f080000;
    }
    public static final class string {
        public static final int action_contact=0x7f060001;
        public static final int app_name=0x7f060000;
    }
    public static final class style {
        public static final int ActionBarStyle=0x7f070002;
        public static final int AppBaseTheme=0x7f070000;
        public static final int AppTheme=0x7f070001;
    }
    public static final class styleable {
        /** Attributes that can be used with a PagerSlidingTabStrip.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsDividerColor com.java.net.zendah:pstsDividerColor}</code></td><td></td></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsDividerPadding com.java.net.zendah:pstsDividerPadding}</code></td><td></td></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsIndicatorColor com.java.net.zendah:pstsIndicatorColor}</code></td><td></td></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsIndicatorHeight com.java.net.zendah:pstsIndicatorHeight}</code></td><td></td></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsScrollOffset com.java.net.zendah:pstsScrollOffset}</code></td><td></td></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsShouldExpand com.java.net.zendah:pstsShouldExpand}</code></td><td></td></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsTabBackground com.java.net.zendah:pstsTabBackground}</code></td><td></td></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsTabPaddingLeftRight com.java.net.zendah:pstsTabPaddingLeftRight}</code></td><td></td></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsTextAllCaps com.java.net.zendah:pstsTextAllCaps}</code></td><td></td></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsUnderlineColor com.java.net.zendah:pstsUnderlineColor}</code></td><td></td></tr>
           <tr><td><code>{@link #PagerSlidingTabStrip_pstsUnderlineHeight com.java.net.zendah:pstsUnderlineHeight}</code></td><td></td></tr>
           </table>
           @see #PagerSlidingTabStrip_pstsDividerColor
           @see #PagerSlidingTabStrip_pstsDividerPadding
           @see #PagerSlidingTabStrip_pstsIndicatorColor
           @see #PagerSlidingTabStrip_pstsIndicatorHeight
           @see #PagerSlidingTabStrip_pstsScrollOffset
           @see #PagerSlidingTabStrip_pstsShouldExpand
           @see #PagerSlidingTabStrip_pstsTabBackground
           @see #PagerSlidingTabStrip_pstsTabPaddingLeftRight
           @see #PagerSlidingTabStrip_pstsTextAllCaps
           @see #PagerSlidingTabStrip_pstsUnderlineColor
           @see #PagerSlidingTabStrip_pstsUnderlineHeight
         */
        public static final int[] PagerSlidingTabStrip = {
            0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003,
            0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007,
            0x7f010008, 0x7f010009, 0x7f01000a
        };
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsDividerColor}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a color value, in the form of "<code>#<i>rgb</i></code>", "<code>#<i>argb</i></code>",
"<code>#<i>rrggbb</i></code>", or "<code>#<i>aarrggbb</i></code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.java.net.zendah:pstsDividerColor
        */
        public static final int PagerSlidingTabStrip_pstsDividerColor = 2;
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsDividerPadding}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.java.net.zendah:pstsDividerPadding
        */
        public static final int PagerSlidingTabStrip_pstsDividerPadding = 5;
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsIndicatorColor}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a color value, in the form of "<code>#<i>rgb</i></code>", "<code>#<i>argb</i></code>",
"<code>#<i>rrggbb</i></code>", or "<code>#<i>aarrggbb</i></code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.java.net.zendah:pstsIndicatorColor
        */
        public static final int PagerSlidingTabStrip_pstsIndicatorColor = 0;
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsIndicatorHeight}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.java.net.zendah:pstsIndicatorHeight
        */
        public static final int PagerSlidingTabStrip_pstsIndicatorHeight = 3;
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsScrollOffset}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.java.net.zendah:pstsScrollOffset
        */
        public static final int PagerSlidingTabStrip_pstsScrollOffset = 7;
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsShouldExpand}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.java.net.zendah:pstsShouldExpand
        */
        public static final int PagerSlidingTabStrip_pstsShouldExpand = 9;
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsTabBackground}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.java.net.zendah:pstsTabBackground
        */
        public static final int PagerSlidingTabStrip_pstsTabBackground = 8;
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsTabPaddingLeftRight}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.java.net.zendah:pstsTabPaddingLeftRight
        */
        public static final int PagerSlidingTabStrip_pstsTabPaddingLeftRight = 6;
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsTextAllCaps}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.java.net.zendah:pstsTextAllCaps
        */
        public static final int PagerSlidingTabStrip_pstsTextAllCaps = 10;
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsUnderlineColor}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a color value, in the form of "<code>#<i>rgb</i></code>", "<code>#<i>argb</i></code>",
"<code>#<i>rrggbb</i></code>", or "<code>#<i>aarrggbb</i></code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.java.net.zendah:pstsUnderlineColor
        */
        public static final int PagerSlidingTabStrip_pstsUnderlineColor = 1;
        /**
          <p>This symbol is the offset where the {@link com.java.net.zendah.R.attr#pstsUnderlineHeight}
          attribute's value can be found in the {@link #PagerSlidingTabStrip} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.java.net.zendah:pstsUnderlineHeight
        */
        public static final int PagerSlidingTabStrip_pstsUnderlineHeight = 4;
    };
}
