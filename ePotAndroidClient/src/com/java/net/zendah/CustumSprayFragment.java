/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.java.net.zendah;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

import com.java.net.zendah.ParamsFragment.MyClientTask;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class CustumSprayFragment extends Fragment {

	private static final String ARG_POSITION = "position";
	public static View rootView = null;
	Button bt_spray;
	Global global;

	private int position;

	public static CustumSprayFragment newInstance() {
		CustumSprayFragment f = new CustumSprayFragment();

		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (rootView != null) {
			ViewGroup parent = (ViewGroup) rootView.getParent();
			if (parent != null) {
				parent.removeView(rootView);
			}
		}
		try {
			rootView = inflater
					.inflate(R.layout.custum_spray, container, false);
		} catch (InflateException e) {

		}

		global = (Global) getActivity().getApplication();
		bt_spray = (Button) rootView.findViewById(R.id.bt_spray);
		
		bt_spray.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (global.ipAdress != null && global.port != null) {
					String adress = global.ipAdress;
					int port = Integer.parseInt(global.port);

					MyClientTask myClientTask = new MyClientTask(adress, port,
							2);
					myClientTask.execute();
					

				} else {
					Toast.makeText(getActivity().getApplicationContext(),
							"Please set the e-pot box adresse before",
							Toast.LENGTH_LONG).show();
				}
				
				return true;
			}
		});
		//bt_spray.setOnClickListener();
		// temperature = (ImageView)
		// rootView.findViewById(R.id.temperat_circle);

		return rootView;
	}
	
	public class MyClientTask extends AsyncTask<Void, Void, Void> {

		String dstAddress;
		int dstPort;
		int CMD;
		String response;
		OutputStream outputStream;

		MyClientTask(String addr, int port, int cmd) {
			CMD = cmd;
			dstAddress = addr;
			dstPort = port;
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			try {

				Socket socket = new Socket(dstAddress, dstPort);
				outputStream = socket.getOutputStream();
				try (PrintStream printStream = new PrintStream(outputStream)) {
					printStream.print("" + CMD);
				}

				// InputStream inputStream = socket.getInputStream();
				// ByteArrayOutputStream byteArrayOutputStream = new
				// ByteArrayOutputStream(
				// 1024);
				// byte[] buffer = new byte[1024];
				//
				// int bytesRead;
				// while ((bytesRead = inputStream.read(buffer)) != -1) {
				// byteArrayOutputStream.write(buffer, 0, bytesRead);
				// }

				// socket.close();
				// response = byteArrayOutputStream.toString("UTF-8");

			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//ParamsFragment.newInstance();
//			Fragment prev = getChildFragmentManager().findFragmentByTag("params");
//			if (prev != null) {
//				Log.i("ccccc","dd");
//				DialogFragment df = (DialogFragment) prev;
//				df.dismiss();
//			}
			// Log.i("response ", response);
			// global.setLevies(response);
			super.onPostExecute(result);
		}

	}


}