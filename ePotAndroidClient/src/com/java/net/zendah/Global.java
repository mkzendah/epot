package com.java.net.zendah;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

public class Global extends Application {

	String TAG_LEVY = "last_levy";
	String TAG_DATE = "date";
	String TAG_MOISTURE = "moisture";
	String TAG_LIGHT = "light";
	String TAG_TEMPERATURE = "temperature";

	public String ipAdress;
	public String port;
	public Levy levy;

	public void setLevies(String response) {
		Log.i("response de globale ", response);
		JSONObject jo;
		Levy lv = null;
		try {
			jo = new JSONObject(response);
			JSONObject j2 = jo.getJSONObject("last_levy");
			j2.getInt("light");
			String st2 = j2.getString("light").toString();
			lv = new Levy(j2.getString(TAG_DATE), j2.getString(TAG_MOISTURE),
					j2.getString(TAG_LIGHT), j2.getString(TAG_TEMPERATURE));
			Log.i("lv ", lv.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		levy = lv;
	}

}
