package com.java.net.zendah;

import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.astuetz.PagerSlidingTabStrip.IconTabProvider;

public class QuickContactFragment extends DialogFragment {
	public static final String TAG = QuickContactFragment.class.getSimpleName();


	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private ContactPagerAdapter adapter;
	EditText txt_ip_adress, txt_port;
	Button bt_ok;
	Global global;

	public static QuickContactFragment newInstance() {
		QuickContactFragment f = new QuickContactFragment();
		return f;// Quick Contact
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (getDialog() != null) {
			getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
			getDialog().getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
		}

		View root = inflater.inflate(R.layout.fragment_quick_contact,
				container, false);
		global = (Global) getActivity().getApplication();
		txt_ip_adress = (EditText) root.findViewById(R.id.ipadress);
		txt_port = (EditText) root.findViewById(R.id.port);
		bt_ok = (Button) root.findViewById(R.id.bt_ok_setting);
		bt_ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				global.ipAdress = txt_ip_adress.getText().toString();
				global.port = txt_port.getText().toString();
				Log.d("Test", "onClickListener ist gestartet");
				
				Fragment prev = getChildFragmentManager().findFragmentByTag(
						"QuickContactFragment");
				if (prev != null) {
					DialogFragment df = (DialogFragment) prev;
					df.dismiss();
				}

				//
				// FragmentManager fm = getChildFragmentManager();
				// Fragment fragment_byID =
				// fm.findFragmentById(R.id.frag_contact);
				// FragmentManager manager =
				// getActivity().getSupportFragmentManager();
				// FragmentTransaction trans = manager.beginTransaction();
				// trans.remove(fragment_byID);
				// trans.commit();
				// manager.popBackStack();
				//
				// FragmentTransaction ft = fragmentManager.beginTransaction();
				// ft.replace(R.id.mapContainer, mf);
				// ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				// ft.addToBackStack("map");
				// ft.commit();
				// // saveInString();
				// // getActivity().finish();
				// getActivity().getFragmentManager().popBackStack();
				// getActivity()
				// .getFragmentManager()
				// .beginTransaction()
				// .remove(getActivity().getFragmentManager().getFragment(
				// getArguments(), getTag())).commit();

			}
		});
		// tabs = (PagerSlidingTabStrip) root.findViewById(R.id.tabs);
		// pager = (ViewPager) root.findViewById(R.id.pager);
		// adapter = new ContactPagerAdapter();
		//
		// pager.setAdapter(adapter);
		//
		// tabs.setViewPager(pager);

		return root;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart() {
		super.onStart();

		// change dialog width
		if (getDialog() != null) {

			int fullWidth = getDialog().getWindow().getAttributes().width;

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
				Display display = getActivity().getWindowManager()
						.getDefaultDisplay();
				Point size = new Point();
				display.getSize(size);
				fullWidth = size.x;
			} else {
				Display display = getActivity().getWindowManager()
						.getDefaultDisplay();
				fullWidth = display.getWidth();
			}

			final int padding = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 24, getResources()
							.getDisplayMetrics());

			int w = fullWidth - padding;
			int h = getDialog().getWindow().getAttributes().height;

			getDialog().getWindow().setLayout(w, h);
		}
	}

	public class ContactPagerAdapter extends PagerAdapter implements
			IconTabProvider {

		private final int[] ICONS = { R.drawable.ic_launcher_gplus,
				R.drawable.ic_launcher_gmail, R.drawable.ic_launcher_gmaps,
				R.drawable.ic_launcher_chrome };

		public ContactPagerAdapter() {
			super();
		}

		@Override
		public int getCount() {
			return ICONS.length;
		}

		@Override
		public int getPageIconResId(int position) {
			return ICONS[position];
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			// looks a little bit messy here
			TextView v = new TextView(getActivity());
			v.setBackgroundResource(R.color.background_window);
			v.setText("PAGE " + (position + 1));
			final int padding = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 16, getResources()
							.getDisplayMetrics());
			v.setPadding(padding, padding, padding, padding);
			v.setGravity(Gravity.CENTER);
			container.addView(v, 0);
			return v;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object view) {
			container.removeView((View) view);
		}

		@Override
		public boolean isViewFromObject(View v, Object o) {
			return v == ((View) o);
		}

	}

}
